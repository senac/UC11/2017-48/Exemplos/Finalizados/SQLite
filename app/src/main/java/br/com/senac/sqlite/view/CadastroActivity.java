package br.com.senac.sqlite.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.senac.sqlite.R;
import br.com.senac.sqlite.dao.AlunoDAO;
import br.com.senac.sqlite.model.Aluno;

public class CadastroActivity extends AppCompatActivity {

    private EditText txtNome ;
    private EditText txtTelefone ;
    private EditText txtEmail ;
    private EditText txtSite ;
    private Button btnSalvar ;

    private Aluno aluno ;
    private AlunoDAO dao ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        /* buscando componentes na view */
        txtNome = findViewById(R.id.txtNome);
        txtTelefone = findViewById(R.id.txtTelefone);
        txtEmail = findViewById(R.id.txtEmail);
        txtSite = findViewById(R.id.txtSite);
        btnSalvar = findViewById(R.id.btnSalvar);

        /* Adicionando acao de click no botao  */
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aluno = new Aluno();

                aluno.setNome(txtNome.getText().toString());
                aluno.setTelefone(txtTelefone.getText().toString());
                aluno.setEmail(txtEmail.getText().toString());
                aluno.setSite(txtSite.getText().toString());
               try {
                   dao = new AlunoDAO(CadastroActivity.this);
                   dao.salvar(aluno);
                   dao.close();
                   Toast.makeText(CadastroActivity.this ,
                           "Salvo com sucesso." ,
                           Toast.LENGTH_LONG).show();

                   finish();

               }catch (Exception ex){
                   Toast.makeText(CadastroActivity.this ,
                           "Erro ao salvar." ,
                           Toast.LENGTH_LONG).show();
               }
                
                //salvar no banco
            }
        });
















    }
}
