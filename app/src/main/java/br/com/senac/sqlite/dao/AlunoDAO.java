package br.com.senac.sqlite.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.sqlite.model.Aluno;

public class AlunoDAO  extends SQLiteOpenHelper{


    private static final String DATABASE = "SQLite" ;
    private static final int VERSAO = 1 ;

    public AlunoDAO(Context context) {
        super(context, DATABASE , null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
       String ddl =  "CREATE TABLE ALunos " +
                "( id INTEGER PRIMARY KEY , " +
                "nome TEXT NOT NULL , " +
                "telefone TEXT  ,  " +
                "email TEXT  , " +
                "site TEXT ) ; " ;

       db.execSQL(ddl);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        String ddl  = "DROP TABLE IF EXISTS Alunos ;" ;
        db.execSQL(ddl);
        this.onCreate(db);

    }


    public void salvar(Aluno aluno) {

        ContentValues values = new ContentValues() ;
        values.put("nome" , aluno.getNome());
        values.put("telefone" , aluno.getTelefone());
        values.put("email" , aluno.getEmail());
        values.put("site" , aluno.getSite());



        getWritableDatabase().insert(
                "Alunos" ,
                null ,
                values) ;

    }


    public List<Aluno> getLista() {
        List<Aluno> lista = new ArrayList<>();
        String colunas[] = {"id" , "nome" , "telefone" , "email" , "site"} ;

       Cursor cursor =  getWritableDatabase().query(
                "Alunos",
                colunas,
                null,
                null,
                null,
                null,
                null);

       while(cursor.moveToNext()) {

           Aluno aluno = new Aluno();
           aluno.setId(cursor.getInt(0));
           aluno.setNome(cursor.getString(1));
           aluno.setTelefone(cursor.getString(2));
           aluno.setEmail(cursor.getString(3));
           aluno.setSite(cursor.getString(4));

           lista.add(aluno);

       }


        return lista ;
    }























}
